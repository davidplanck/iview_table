const routers = [{
    path: '/',
    meta: {
        title: ''
    },
    component: (resolve) => require(['./views/index.vue'], resolve)
},
    {
    path: '/table',
    meta: {
        title: 'table'
    },
    component: (resolve) => require(['./views/myTable.vue'], resolve)
}

];
export default routers;